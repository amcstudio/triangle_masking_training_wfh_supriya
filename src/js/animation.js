'use strict';

~ (function() {
    var $ = gsap,
       ad = document.getElementById('mainContent'),
       currentLoop=1,
       loops=3,
       bgExit = document.getElementById('bgExit');

    window.init = function() {
        ad.style.display = 'block';
        bgExit.addEventListener('click',exitHandler,false);

        play();
          
    }   
    
    function play() {
        var tl = gsap.timeline();
        tl.addLabel('frameOne')
        tl.to("#gradient",{duration:1, rotation:-72,ease:'sine.inOut'},'frameOne+=3');

    }

    function exitHandler(e){
        e.preventDefault();
        window.open(window.clickTag);
    }

}) ();

window.onload = init();